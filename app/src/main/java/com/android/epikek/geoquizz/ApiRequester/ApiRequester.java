package com.android.epikek.geoquizz.ApiRequester;

import android.os.AsyncTask;

import com.android.epikek.geoquizz.Countries.Country;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import io.realm.Realm;

/**
 * Created by fabca on 27/04/2017.
 */

public class ApiRequester extends AsyncTask<String, Integer, String> {

    private HttpClient client = HttpClientBuilder.create().build();
    private final String USER_AGENT = "Mozilla/5.0";
    private final String ApiUrl = "https://restcountries.eu/rest/v2";
    private final String RestUrl = "https://restcountries.eu/rest/v2";

    public String updateDb() throws Exception {
        HttpGet get = new HttpGet(RestUrl + "/all");
        int responseCode;
        HttpResponse response;
        StringBuffer result = new StringBuffer();
        String line;
        List<Country> countries = new ArrayList<>();
        Gson g = new Gson();

        get.setHeader("User-Agent", USER_AGENT);

        response = client.execute(get);
        responseCode = response.getStatusLine().getStatusCode();

        if (responseCode != 200) {
            return "Couldn't join the db servers.";
        }

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        Realm realm = Realm.getDefaultInstance();

        try {
            JSONArray jsonArray = new JSONArray(result.toString());
            for(int n = 0; n < jsonArray.length(); n++)
            {
                Country toAdd = g.fromJson(jsonArray.getString(n), Country.class);
                if (!countries.contains(toAdd)) {
                    countries.add(toAdd);
                }
            }
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(countries);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Db has been updated";
    }

    public String getContent(String url) throws Exception {
        HttpGet get = new HttpGet(ApiUrl + url);
        int responseCode;
        HttpResponse response;
        StringBuffer result = new StringBuffer();
        String line;

        get.setHeader("User-Agent", USER_AGENT);

        response = client.execute(get);
        responseCode = response.getStatusLine().getStatusCode();
        System.out.println("\nSending 'GET' request to URL : " + ApiUrl + url);
        System.out.println("Response Code : " + responseCode);

        System.out.println(response.toString());
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }

    @Override
    protected String doInBackground(String... params) {
        String result;

        switch (params[0]) {
            case "get":
                try {
                    result = getContent(params[1]);
                } catch (Exception e) {
                    result = null;
                }
                break;
            case "updateDb":
                try {
                    result = updateDb();
                } catch (Exception e) {
                    result = "Couldn't update DB";
                }
                break;
            default:
                result = "Http Method not found.";
        }
        return result;
    }
}
