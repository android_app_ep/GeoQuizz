package com.android.epikek.geoquizz.Countries;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by fabca on 27/04/2017.
 */

public class Country extends RealmObject implements Serializable {
    @PrimaryKey
    @Required
    private String name;
    private String capital;
    private String region;
    private String alpha2Code;
    private String flag;

    public Country() {
    }

    public Country(String name, String capital, String region, String alpha2Code, String flag) {
        this.name = name;
        this.capital = capital;
        this.region = region;
        this.alpha2Code = alpha2Code;
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAlpha2Code() {
        return alpha2Code;
    }

    public void setAlpha2Code(String alpha2Code) {
        this.alpha2Code = alpha2Code;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
