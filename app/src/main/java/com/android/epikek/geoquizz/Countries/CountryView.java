package com.android.epikek.geoquizz.Countries;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.epikek.geoquizz.ImageDownloader.ImageDownloader;
import com.android.epikek.geoquizz.R;
import com.android.epikek.geoquizz.Users.User;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class CountryView extends AppCompatActivity {

    private final String flagUrl = "http://flagpedia.net/data/flags/w580/";
    //private TextView cInfo;
    private ListView cListView;
    private EditText cChoice;
    private Button select;
    private ImageView fView;
    private  countryListViewAdapter countryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_view);

        Realm realm = Realm.getDefaultInstance();

        cChoice = (EditText)findViewById(R.id.countryChoice);
        cChoice.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                System.out.println("Text ["+s+"]");

                countryAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        cListView = (ListView) findViewById(R.id.country_list_view);

        RealmResults<Country> countries = realm.where(Country.class).findAll();

        final ArrayList<Country> countryList = new ArrayList<>();
        countryList.addAll(realm.copyFromRealm(countries));

        String[] listItems = new String[countryList.size()];

        for(int i = 0; i < countryList.size(); i++){
            Country country = countryList.get(i);
            listItems[i] = country.getName();
        }

        countryAdapter = new countryListViewAdapter(this, countryList);
        cListView.setAdapter(countryAdapter);
        final Context context = this;
        cListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Country selectedCountry = countryList.get(position);

                Intent detailIntent = new Intent(context, detailsActivity.class);

                detailIntent.putExtra("country", selectedCountry);

                startActivity(detailIntent);
            }

        });
    }
}
