package com.android.epikek.geoquizz.Countries;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.epikek.geoquizz.ImageDownloader.ImageDownloader;
import com.android.epikek.geoquizz.R;
//import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by gastonrivoire on 04/05/17.
 */

public class countryListViewAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater mInflater;
        private ArrayList<Country> countryMainList;
        private ArrayList<Country> countrySecList;
        private ItemFilter itemFilter = new ItemFilter();
        private final String flagUrl = "http://flagpedia.net/data/flags/w580/";

        public countryListViewAdapter(Context context, ArrayList<Country> list) {
            super();
            this.context = context;
            countryMainList = list;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return countryMainList.size();
        }

        @Override
        public Object getItem(int position) {
            return countryMainList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = mInflater.inflate(R.layout.list_item_country, parent, false);
            TextView titleTextView =
                    (TextView) rowView.findViewById(R.id.country_list_title);

            TextView subtitleTextView =
                    (TextView) rowView.findViewById(R.id.country_list_subtitle);

            ImageView ImageView =
                    (ImageView) rowView.findViewById(R.id.country_list_thumbnail);

            Country country = (Country) getItem(position);
            ImageView.setTag(country);
            titleTextView.setText(country.getName());
            subtitleTextView.setText(country.getCapital());

            try {
                Drawable d = new ImageDownloader().execute(flagUrl + country.getAlpha2Code().toLowerCase() + ".png", country.getName()).get();
                System.out.println("country.getAlpha2Code() : "+ country.getAlpha2Code().toLowerCase());
                ImageView.setImageDrawable(d);
            } catch (Exception e) {
                System.out.println("image error ");
                e.printStackTrace();
            }


            return rowView;
        }

    public Filter getFilter() {
        return itemFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<Country> list = countryMainList;

            int count = list.size();
            final ArrayList<Country> nlist = new ArrayList<Country>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            countrySecList = (ArrayList<Country>) results.values;
            notifyDataSetChanged();
        }

    }
}
