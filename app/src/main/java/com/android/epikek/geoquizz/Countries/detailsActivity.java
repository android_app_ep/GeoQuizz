package com.android.epikek.geoquizz.Countries;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.epikek.geoquizz.ImageDownloader.ImageDownloader;
import com.android.epikek.geoquizz.R;

public class detailsActivity extends AppCompatActivity {

    private ImageView imageView;
    private final String flagUrl = "http://flagpedia.net/data/flags/w580/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Country countryData = (Country) this.getIntent().getSerializableExtra("country");

        TextView countryLabel = (TextView) findViewById(R.id.countryLabel);
        TextView capitalLabel = (TextView) findViewById(R.id.capitalLabel);
        TextView regionLabel = (TextView) findViewById(R.id.regionLabel);
        imageView = (ImageView) findViewById(R.id.imageView);

        countryLabel.setText(countryData.getName());
        capitalLabel.setText(countryData.getCapital());
        regionLabel.setText(countryData.getRegion());
        try {
            Drawable d = new ImageDownloader().execute(flagUrl + countryData.getAlpha2Code().toLowerCase() + ".png", countryData.getName()).get();
            System.out.println("country.getAlpha2Code() : "+ countryData.getAlpha2Code().toLowerCase());
            imageView.setImageDrawable(d);
        } catch (Exception e) {
            System.out.println("image error ");
            e.printStackTrace();
        }
    }
}
