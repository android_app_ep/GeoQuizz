package com.android.epikek.geoquizz.ImageDownloader;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by fabca on 01/05/2017.
 */

public class ImageDownloader extends AsyncTask<String, Integer, Drawable> {
    @Override
    protected Drawable doInBackground(String... params) {
        try {
            InputStream is = (InputStream) new URL(params[0]).getContent();
            Drawable d = Drawable.createFromStream(is, params[1]);
            return d;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
