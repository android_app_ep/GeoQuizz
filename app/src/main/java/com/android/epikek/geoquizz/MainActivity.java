package com.android.epikek.geoquizz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.epikek.geoquizz.ApiRequester.ApiRequester;
import com.android.epikek.geoquizz.Countries.CountryView;
import com.android.epikek.geoquizz.Quizz.QuizzView;
import com.android.epikek.geoquizz.Scores.Score;
import com.android.epikek.geoquizz.Users.User;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    private ApiRequester requester = new ApiRequester();
    private Realm realm;
    private Button countries;
    private Button quizz;
    private Button updateDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Realm.init(this);
        realm = Realm.getDefaultInstance();
        countries = (Button)findViewById(R.id.countriesView);
        countries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CountryView.class);
                startActivity(intent);
            }
        });
        quizz = (Button)findViewById(R.id.quizzView);
        quizz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, QuizzView.class);
                startActivity(intent);
            }
        });
        updateDb = (Button)findViewById(R.id.updateDb);
        updateDb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requester.execute("updateDb");
            }
        });

        RealmResults<User> res = realm.where(User.class).findAll();
        try {
            if (res.size() == 0) {
                requester.execute("updateDb").get();
                User newUser = new User();
                newUser.setId(1);
                newUser.setSelected(null);
                newUser.setActualScore(new Score());
                newUser.setBestScore(new Score());
                realm.beginTransaction();
                realm.copyToRealm(newUser);
                realm.commitTransaction();
                System.out.println("New user created !");
            } else {
                System.out.println("User restored !");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
