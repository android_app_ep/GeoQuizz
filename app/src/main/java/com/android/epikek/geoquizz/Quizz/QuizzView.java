package com.android.epikek.geoquizz.Quizz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.epikek.geoquizz.Countries.Country;
import com.android.epikek.geoquizz.MainActivity;
import com.android.epikek.geoquizz.R;
import com.android.epikek.geoquizz.Users.User;

import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;

public class QuizzView extends AppCompatActivity {

    private Realm realm;
    private TextView question;
    private TextView answer;
    private RadioButton[] choices;
    private TextView actualScore;
    private TextView highScore;
    private Button back;
    private Country country;
    private User user;
    private int aScore = 0;
    private int hScore;
    private Random random = new Random();

    public void fillQuestion() {
        realm.beginTransaction();
        RealmResults<Country> list = realm.where(Country.class).findAll();
        realm.commitTransaction();
        country = list.get(random.nextInt(list.size()));
        question.setText("What is the capital of :\n" + country.getName() + " ?");
        actualScore.setText("Score: " + aScore);
        highScore.setText("Highscore: " + hScore);
    }

    public void fillChoices() {
        int good = random.nextInt(3);
        int i = 0;
        Country c;

        while (i < 4) {
            choices[i].setActivated(false);
            if (i == good) {
                choices[i++].setText(country.getCapital());
            } else {
                realm.beginTransaction();
                RealmResults<Country> list = realm.where(Country.class)
                        .notEqualTo("name", country.getName()).findAll();
                realm.commitTransaction();
                c = list.get(random.nextInt(list.size()));
                choices[i++].setText(c.getCapital());
            }
        }
    }

    public void updateUserHighScore() {
        if (aScore > hScore) {
            hScore = aScore;
            user.getBestScore().setTotalScore(hScore);
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();
        }
        actualScore.setText("Score: " + aScore);
        highScore.setText("Highscore: " + hScore);
    }

    public void createQuizzOnClickListener() {
        choices[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(choices[0].getText() + " "  + country.getCapital());
                if (choices[0].getText().equals(country.getCapital())) {
                    aScore += 1;
                    answer.setText("Correct ! +1 point !");
                } else {
                    aScore = 0;
                    answer.setText("The correct answer was :\n" + country.getCapital());
                }
                choices[0].setChecked(false);
                updateUserHighScore();
                fillQuestion();
                fillChoices();
            }
        });

        choices[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(choices[1].getText() + " "  + country.getCapital());
                if (choices[1].getText().equals(country.getCapital())) {
                    aScore += 1;
                    answer.setText("Correct ! +1 point !");
                } else {
                    aScore = 0;
                    answer.setText("The correct answer was :\n" + country.getCapital());
                }
                choices[1].setChecked(false);
                updateUserHighScore();
                fillQuestion();
                fillChoices();
            }
        });

        choices[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(choices[2].getText() + " "  + country.getCapital());
                if (choices[2].getText().equals(country.getCapital())) {
                    aScore += 1;
                    answer.setText("Correct ! +1 point !");
                } else {
                    aScore = 0;
                    answer.setText("The correct answer was :\n" + country.getCapital());
                }
                choices[2].setChecked(false);
                updateUserHighScore();
                fillQuestion();
                fillChoices();
            }
        });

        choices[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(choices[3].getText() + " "  + country.getCapital());
                if (choices[3].getText().equals(country.getCapital())) {
                    aScore += 1;
                    answer.setText("Correct ! +1 point !");
                } else {
                    aScore = 0;
                    answer.setText("The correct answer was :\n" + country.getCapital());
                }
                choices[3].setChecked(false);
                updateUserHighScore();
                fillQuestion();
                fillChoices();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizz_view);

        realm = Realm.getDefaultInstance();

        answer = (TextView)findViewById(R.id.answerView);
        back = (Button)findViewById(R.id.backToMenu);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizzView.this, MainActivity.class);
                startActivity(intent);
            }
        });

        question = (TextView)findViewById(R.id.quizzQuestion);
        choices = new RadioButton[4];
        choices[0] = (RadioButton)findViewById(R.id.choice1);
        choices[1] = (RadioButton)findViewById(R.id.choice2);
        choices[2] = (RadioButton)findViewById(R.id.choice3);
        choices[3] = (RadioButton)findViewById(R.id.choice4);
        createQuizzOnClickListener();
        realm.beginTransaction();
        User tUser = realm.where(User.class).findFirst();
        user = realm.copyFromRealm(tUser);
        realm.commitTransaction();

        hScore = user.getBestScore().getTotalScore();
        actualScore = (TextView)findViewById(R.id.actualScore);
        highScore = (TextView)findViewById(R.id.highScore);

        fillQuestion();
        fillChoices();
    }
}
