package com.android.epikek.geoquizz.Scores;

import io.realm.RealmObject;

/**
 * Created by fabca on 30/04/2017.
 */

public class Score extends RealmObject {

    private int totalScore;
    private int afScore;
    private int amScore;
    private int asScore;
    private int euScore;
    private int ocScore;

    public Score() {
        this.totalScore = 0;
        this.afScore = 0;
        this.amScore = 0;
        this.asScore = 0;
        this.euScore = 0;
        this.ocScore = 0;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public int getAfScore() {
        return afScore;
    }

    public void setAfScore(int afScore) {
        this.afScore = afScore;
    }

    public int getAmScore() {
        return amScore;
    }

    public void setAmScore(int amScore) {
        this.amScore = amScore;
    }

    public int getAsScore() {
        return asScore;
    }

    public void setAsScore(int asScore) {
        this.asScore = asScore;
    }

    public int getEuScore() {
        return euScore;
    }

    public void setEuScore(int euScore) {
        this.euScore = euScore;
    }

    public int getOcScore() {
        return ocScore;
    }

    public void setOcScore(int ocScore) {
        this.ocScore = ocScore;
    }

    public int updateTotal() {
        this.totalScore = this.afScore + this.amScore + this.asScore + this.euScore + this.ocScore;
        return this.totalScore;
    }
}
