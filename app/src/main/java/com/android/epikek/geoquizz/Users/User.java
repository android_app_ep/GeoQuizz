package com.android.epikek.geoquizz.Users;

import com.android.epikek.geoquizz.Countries.Country;
import com.android.epikek.geoquizz.Scores.Score;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by fabca on 30/04/2017.
 */

public class User extends RealmObject {
    @PrimaryKey
    private Integer id;
    private Country selected;
    private Score actualScore;
    private Score bestScore;

    public User() {
    }

    public User(Integer id, Country selected, Score actualScore, Score bestScore) {
        this.id = id;
        this.selected = selected;
        this.actualScore = actualScore;
        this.bestScore = bestScore;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Country getSelected() {
        return selected;
    }

    public void setSelected(Country selected) {
        this.selected = selected;
    }

    public Score getActualScore() {
        return actualScore;
    }

    public void setActualScore(Score actualScore) {
        this.actualScore = actualScore;
    }

    public Score getBestScore() {
        return bestScore;
    }

    public void setBestScore(Score bestScore) {
        this.bestScore = bestScore;
    }
}
